package org.example.back_mg_apijson.entity;


import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@TableName("user")
@Data
public class UserEntity {
    @TableId
    private Long id;
    private String name;
    private String password;
    private Integer right;
    static class Right{
        static int ADMIN=0;
        static int USER=1;
    }
}
