package org.example.back_mg_apijson.service.impl;

import org.example.back_mg_apijson.common.service.impl.BaseServiceImpl;
import org.example.back_mg_apijson.dao.UserDao;
import org.example.back_mg_apijson.entity.UserEntity;
import org.example.back_mg_apijson.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends BaseServiceImpl<UserDao, UserEntity> implements UserService {

}
