package org.example.back_mg_apijson.service;

import org.example.back_mg_apijson.common.page.PageResult;
import org.example.back_mg_apijson.common.query.Query;
import org.example.back_mg_apijson.common.service.BaseService;
import org.example.back_mg_apijson.entity.BaseClassEntity;
import org.example.back_mg_apijson.entity.TableEntity;

/**
 * 数据表
 *
 * @author 阿沐 babamu@126.com
 */
public interface TableService extends BaseService<TableEntity> {

    PageResult<TableEntity> page(Query query);

    TableEntity getByTableName(String tableName);

    void deleteBatchIds(Long[] ids);

    /**
     * 导入表
     *
     * @param datasourceId 数据源ID
     * @param tableName    表名
     */
    void tableImport(Long datasourceId, String tableName);

    /**
     * 同步数据库表
     *
     * @param id 表ID
     */
    void sync(Long id);
}