package org.example.back_mg_apijson.service;


import org.example.back_mg_apijson.common.page.PageResult;
import org.example.back_mg_apijson.common.query.Query;
import org.example.back_mg_apijson.common.service.BaseService;
import org.example.back_mg_apijson.entity.BaseClassEntity;

import java.util.List;

/**
 * 基类管理
 *
 * @author 阿沐 babamu@126.com
 */
public interface BaseClassService extends BaseService<BaseClassEntity> {

    PageResult<BaseClassEntity> page(Query query);

    List<BaseClassEntity> getList();
}