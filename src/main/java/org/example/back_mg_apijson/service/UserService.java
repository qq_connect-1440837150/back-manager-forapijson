package org.example.back_mg_apijson.service;

import org.example.back_mg_apijson.common.service.BaseService;
import org.example.back_mg_apijson.entity.UserEntity;

public interface UserService extends BaseService<UserEntity> {
}
