package org.example.back_mg_apijson.service;

import org.example.back_mg_apijson.common.page.PageResult;
import org.example.back_mg_apijson.common.query.Query;
import org.example.back_mg_apijson.common.service.BaseService;
import org.example.back_mg_apijson.entity.BaseClassEntity;
import org.example.back_mg_apijson.entity.FieldTypeEntity;

import java.util.Map;
import java.util.Set;

/**
 * 字段类型管理
 *
 * @author 阿沐 babamu@126.com
 */
public interface FieldTypeService extends BaseService<FieldTypeEntity> {
    PageResult<FieldTypeEntity> page(Query query);

    Map<String, FieldTypeEntity> getMap();

    /**
     * 根据tableId，获取包列表
     *
     * @param tableId 表ID
     * @return 返回包列表
     */
    Set<String> getPackageByTableId(Long tableId);

    Set<String> getList();
}