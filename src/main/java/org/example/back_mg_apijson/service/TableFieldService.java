package org.example.back_mg_apijson.service;

import org.example.back_mg_apijson.common.page.PageResult;
import org.example.back_mg_apijson.common.query.Query;
import org.example.back_mg_apijson.common.service.BaseService;
import org.example.back_mg_apijson.entity.BaseClassEntity;
import org.example.back_mg_apijson.entity.TableFieldEntity;

import java.util.List;

/**
 * 表字段
 *
 * @author 阿沐 babamu@126.com
 */
public interface TableFieldService extends BaseService<TableFieldEntity> {

    List<TableFieldEntity> getByTableId(Long tableId);

    void deleteBatchTableIds(Long[] tableIds);

    /**
     * 修改表字段数据
     *
     * @param tableId        表ID
     * @param tableFieldList 字段列表
     */
    void updateTableField(Long tableId, List<TableFieldEntity> tableFieldList);

    /**
     * 初始化字段数据
     */
    void initFieldList(List<TableFieldEntity> tableFieldList);
}