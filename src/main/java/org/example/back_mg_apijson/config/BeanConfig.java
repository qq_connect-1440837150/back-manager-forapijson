package org.example.back_mg_apijson.config;

import org.example.back_mg_apijson.config.template.GeneratorConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfig {
    @Bean
    GeneratorConfig generatorConfig() {
        return new GeneratorConfig("/template/");
    }
}
