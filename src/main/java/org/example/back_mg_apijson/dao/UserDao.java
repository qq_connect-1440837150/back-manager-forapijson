package org.example.back_mg_apijson.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.example.back_mg_apijson.entity.UserEntity;

@Mapper
public interface UserDao extends BaseMapper<UserEntity> {
}
