package org.example.back_mg_apijson.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.example.back_mg_apijson.entity.TableFieldEntity;

import java.util.List;

/**
 * 表字段
 *
 * @author 阿沐 babamu@126.com
 */
@Mapper
public interface TableFieldDao extends BaseMapper<TableFieldEntity> {

    List<TableFieldEntity> getByTableId(Long tableId);

    void deleteBatchTableIds(Long[] tableIds);
}
