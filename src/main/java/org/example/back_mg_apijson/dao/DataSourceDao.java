package org.example.back_mg_apijson.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.example.back_mg_apijson.entity.DataSourceEntity;

/**
 * 数据源管理
 *
 * @author 阿沐 babamu@126.com
 */
@Mapper
public interface DataSourceDao extends BaseMapper<DataSourceEntity> {
	
}