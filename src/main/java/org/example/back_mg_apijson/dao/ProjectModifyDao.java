package org.example.back_mg_apijson.dao;

import org.apache.ibatis.annotations.Mapper;
import org.example.back_mg_apijson.entity.ProjectModifyEntity;

/**
 * 项目名变更
 *
 * @author 阿沐 babamu@126.com
 */
@Mapper
public interface ProjectModifyDao extends com.baomidou.mybatisplus.core.mapper.BaseMapper<ProjectModifyEntity> {

}