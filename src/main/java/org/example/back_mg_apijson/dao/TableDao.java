package org.example.back_mg_apijson.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.example.back_mg_apijson.entity.TableEntity;

/**
 * 数据表
 *
 * @author 阿沐 babamu@126.com
 */
@Mapper
public interface TableDao extends BaseMapper<TableEntity> {

}
