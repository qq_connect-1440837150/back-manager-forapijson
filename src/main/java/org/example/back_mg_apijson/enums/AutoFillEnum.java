package org.example.back_mg_apijson.enums;

/**
 * 字段自动填充 枚举
 *
 * @author 阿沐 babamu@126.com
 */
public enum AutoFillEnum {
    DEFAULT, INSERT, UPDATE, INSERT_UPDATE;
}