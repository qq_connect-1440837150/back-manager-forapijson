package org.example.back_mg_apijson;

import apijson.framework.APIJSONApplication;
import org.example.back_mg_apijson.apijson.DemoAPIJSONCreator;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@ServletComponentScan
@MapperScan({"org.example.back_mg_apijson.dao",
        "org.example.back_mg_apijson.common.dao"})
public class RunApplication {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(RunApplication.class, args);

    }

}
