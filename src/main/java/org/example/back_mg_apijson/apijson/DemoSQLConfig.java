package org.example.back_mg_apijson.apijson;

import apijson.RequestMethod;
import apijson.framework.APIJSONSQLConfig;
import apijson.orm.AbstractSQLConfig;
import apijson.orm.Join;
import apijson.orm.Join.On;
import com.alibaba.fastjson.annotation.JSONField;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

import static apijson.framework.APIJSONConstant.*;


/**SQL配置
 * TiDB 用法和 MySQL 一致
 * 具体见详细的说明文档 C.开发说明 C-1-1.修改数据库链接
 * https://github.com/Tencent/APIJSON/6.2523.blob/master/%E8%AF%A6%E7%BB%86%E7%9A%84%E8%AF%B4%E6%98%8E%E6%96%87%E6%A1%A3.md#c-1-1%E4%BF%AE%E6%94%B9%E6%95%B0%E6%8D%AE%E5%BA%93%E9%93%BE%E6%8E%A5
 * @author Lemon
 */
@Component
public class DemoSQLConfig extends APIJSONSQLConfig {
	@Value("${apijson.dataSource.url}")
	private String connectString;
	@Value("${apijson.dataSource.userName}")
	private String userName;
	@Value("${apijson.dataSource.password}")
	private String password;
	@Value("${apijson.dataSource.database}")
	private String database;
	private static String CONNECTSTRING;
	private static String USERNAME;
	private static String PASSWORD;
	private static String DATABASE;

	@PostConstruct
	public void init(){
		DEFAULT_DATABASE = DATABASE_MYSQL;  //TODO 默认数据库类型，改成你自己的。TiDB, MariaDB, OceanBase 这类兼容 MySQL 的可当做 MySQL 使用
		DEFAULT_SCHEMA = "maku_generator";  //TODO 默认数据库名/模式，改成你自己的，默认情况是 MySQL: sys, PostgreSQL: sys, SQL Server: dbo, Oracle:
		// 主键名映射
		SIMPLE_CALLBACK = new SimpleCallback<Long>() {

			@Override
			public AbstractSQLConfig getSQLConfig(RequestMethod method, String database, String schema, String datasource, String table) {
				return new DemoSQLConfig(method, table);
			}


			@Override
			public String getUserIdKey(String database, String schema, String datasource, String table) {
				return USER_.equals(table) || PRIVACY_.equals(table) ? ID : USER_ID; // id / userId
			}

			// 取消注释来实现数据库自增 id
			@Override
			public Long newId(RequestMethod method, String database, String schema, String datasource, String table) {
				return null; // return null 则不生成 id，一般用于数据库自增 id
			}
		};
		CONNECTSTRING = this.connectString;
		USERNAME = this.userName;
		PASSWORD = this.password;
		DATABASE = this.database;

	}
	public DemoSQLConfig() {
		super();


	}
	public DemoSQLConfig(RequestMethod method, String table) {
		super(method, table);
	}




	// 如果 DemoSQLExecutor.getConnection 能拿到连接池的有效 Connection，则这里不需要配置 dbVersion, dbUri, dbAccount, dbPassword

	@Override
	public String getDBVersion() {
		return "5.7.22"; //"8.0.11"; //TODO 改成你自己的 MySQL 或 PostgreSQL 数据库版本号 //MYSQL 8 和 7 使用的 JDBC 配置不一样
	}

	@JSONField(serialize = false)  // 不在日志打印 账号/密码 等敏感信息，用了 UnitAuto 则一定要加
	@Override
	public String getDBUri() {
		return CONNECTSTRING; //TODO 改成你自己的，TiDB 可以当成 MySQL 使用，默认端口为 4000
	}

	@JSONField(serialize = false)  // 不在日志打印 账号/密码 等敏感信息，用了 UnitAuto 则一定要加
	@Override
	public String getDBAccount() {
		return USERNAME;  //TODO 改成你自己的
	}

	@JSONField(serialize = false)  // 不在日志打印 账号/密码 等敏感信息，用了 UnitAuto 则一定要加
	@Override
	public String getDBPassword() {
		return PASSWORD;  //TODO 改成你自己的，TiDB 可以当成 MySQL 使用， 默认密码为空字符串 ""
	}
	@Override
	protected void onGetCrossJoinString(Join j) throws UnsupportedOperationException {
		// 开启 CROSS JOIN 笛卡尔积联表  	super.onGetCrossJoinString(j);
	}
	@Override
	protected void onJoinNotRelation(String sql, String quote, Join j, String jt, List<On> onList, On on) {
		// 开启 JOIN	ON t1.c1 != t2.c2 等不等式关联 	super.onJoinNotRelation(sql, quote, j, jt, onList, on);
	}
	@Override
	protected void onJoinComplextRelation(String sql, String quote, Join j, String jt, List<On> onList, On on) {
		// 开启 JOIN	ON t1.c1 LIKE concat('%', t2.c2, '%') 等复杂关联		super.onJoinComplextRelation(sql, quote, j, jt, onList, on);
	}

}
