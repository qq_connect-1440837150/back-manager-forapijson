package org.example.back_mg_apijson.apijson;

import apijson.framework.APIJSONApplication;
import lombok.extern.log4j.Log4j;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
@Log4j2
public class RunApp {
    @Autowired
    private DemoAPIJSONCreator demoAPIJSONCreator;
    @PostConstruct
    public void init() throws Exception {

        log.info("==================apijson初始化==================");
        APIJSONApplication.init(demoAPIJSONCreator);
    }
}
