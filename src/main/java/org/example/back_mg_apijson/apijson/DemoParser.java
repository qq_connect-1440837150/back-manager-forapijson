package org.example.back_mg_apijson.apijson;

import apijson.RequestMethod;
import apijson.framework.APIJSONParser;

public class DemoParser extends APIJSONParser<Long> {
    public DemoParser() {
        super();
    }

    public DemoParser(RequestMethod method) {
        super(method);
    }

    public DemoParser(RequestMethod method, boolean needVerify) {
        super(method, needVerify);
    }
}
