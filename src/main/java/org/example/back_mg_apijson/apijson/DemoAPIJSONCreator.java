package org.example.back_mg_apijson.apijson;

import apijson.framework.APIJSONCreator;
import apijson.orm.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DemoAPIJSONCreator extends APIJSONCreator {
    public DemoAPIJSONCreator() {

    }

    @Autowired
    private DemoSQLConfig demoSQLConfig;
    @Override
    public Parser createParser() {
        return super.createParser();
    }

    @Override
    public FunctionParser createFunctionParser() {
        return super.createFunctionParser();
    }

    @Override
    public Verifier createVerifier() {
        return super.createVerifier();
    }

    @Override
    public SQLConfig createSQLConfig() {
        return demoSQLConfig;
    }

    @Override
    public SQLExecutor createSQLExecutor() {
        return super.createSQLExecutor();
    }
}
