package org.example.back_mg_apijson.util;


import org.example.back_mg_apijson.config.DbType;
import org.example.back_mg_apijson.config.GenDataSource;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * DB工具类
 *
 * @author 阿沐 babamu@126.com
 */
public class DbUtils {
    private static final int CONNECTION_TIMEOUTS_SECONDS = 6;

    /**
     * 获得数据库连接
     */
    public static Connection getConnection(GenDataSource dataSource) throws ClassNotFoundException, SQLException {
        DriverManager.setLoginTimeout(CONNECTION_TIMEOUTS_SECONDS);
        Class.forName(dataSource.getDbType().getDriverClass());

        Connection connection = DriverManager.getConnection(dataSource.getConnUrl(), dataSource.getUsername(), dataSource.getPassword());
        // todo : 暂时不考虑其他数据库
//        if (dataSource.getDbType() == DbType.Oracle) {
//            ((OracleConnection) connection).setRemarksReporting(true);
//        }

        return connection;
    }


}