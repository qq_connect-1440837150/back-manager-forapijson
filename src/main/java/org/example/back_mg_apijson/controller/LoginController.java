package org.example.back_mg_apijson.controller;

import cn.hutool.jwt.JWTUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.example.back_mg_apijson.common.utils.Result;
import org.example.back_mg_apijson.entity.UserEntity;
import org.example.back_mg_apijson.service.UserService;
import org.example.back_mg_apijson.util.ConstUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
public class LoginController {
    @Autowired
    private UserService userService;
    @GetMapping
    public Result login(String userName, String password){
        QueryWrapper<UserEntity> userWrapper = new QueryWrapper<>();
        userWrapper.eq("name",userName);
        UserEntity user = userService.getOne(userWrapper);
        if(user.getPassword().equals(password)){
            HashMap<String, Object> stringObjectHashMap = new HashMap<>();
            stringObjectHashMap.put(ConstUtil.USER,user.getName());
            stringObjectHashMap.put(ConstUtil.USER_ID,user.getId());
            stringObjectHashMap.put(ConstUtil.RIGHT,user.getRight());
            String token = JWTUtil.createToken(stringObjectHashMap, "12345".getBytes());
            HashMap<String, Object> reHash = new HashMap<>();
            reHash.put("token",token);
            reHash.put("right",user.getRight());
            reHash.put("name",user.getName());
            return Result.ok(reHash);
        }else{
            return Result.error("账号密码错误");
        }
    }
}
