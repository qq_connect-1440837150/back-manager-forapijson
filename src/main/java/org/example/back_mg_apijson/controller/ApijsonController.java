package org.example.back_mg_apijson.controller;

import apijson.RequestMethod;
import com.alibaba.fastjson.JSONObject;
import org.example.back_mg_apijson.apijson.DemoParser;
import org.springframework.web.bind.annotation.*;

@RestController("/apijson")
@RequestMapping("/apijson")
public class ApijsonController {
    @PostMapping("/admin/{method}")
    public Object adminPost(@PathVariable("method")String method, @RequestBody String request) {
        JSONObject jsonObject = new DemoParser(RequestMethod.valueOf(method.toUpperCase())).
                setNeedVerify(false).parseResponse(request);
        return jsonObject;
    }
}
