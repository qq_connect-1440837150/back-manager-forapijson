/*
 Navicat Premium Data Transfer

 Source Server         : gz-cynosdbmysql-grp-q0u8k5ub.sql.tencentcdb.com
 Source Server Type    : MySQL
 Source Server Version : 50718
 Source Host           : gz-cynosdbmysql-grp-q0u8k5ub.sql.tencentcdb.com:20559
 Source Schema         : maku_generator

 Target Server Type    : MySQL
 Target Server Version : 50718
 File Encoding         : 65001

 Date: 14/12/2022 12:31:35
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for Access
-- ----------------------------
DROP TABLE IF EXISTS `Access`;
CREATE TABLE `Access`  (
  `id` bigint(15) NOT NULL AUTO_INCREMENT,
  `debug` tinyint(2) NOT NULL DEFAULT 0 COMMENT '是否为 DEBUG 调试数据，只允许在开发环境使用，测试和线上环境禁用：0-否，1-是。',
  `schema` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据库名/模式',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '实际表名，例如 apijson_user',
  `alias` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '外部调用的表别名，例如 User，前端传参示例 { \"User\":{} }',
  `get` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '[\"UNKNOWN\", \"LOGIN\", \"CONTACT\", \"CIRCLE\", \"OWNER\", \"ADMIN\"]' COMMENT '允许 get 的角色列表，例如 [\"LOGIN\", \"CONTACT\", \"CIRCLE\", \"OWNER\"]\n用 JSON 类型不能设置默认值，反正权限对应的需求是明确的，也不需要自动转 JSONArray。\nTODO: 直接 LOGIN,CONTACT,CIRCLE,OWNER 更简单，反正是开发内部用，不需要复杂查询。',
  `head` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '[\"UNKNOWN\", \"LOGIN\", \"CONTACT\", \"CIRCLE\", \"OWNER\", \"ADMIN\"]' COMMENT '允许 head 的角色列表，例如 [\"LOGIN\", \"CONTACT\", \"CIRCLE\", \"OWNER\"]',
  `gets` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '[\"LOGIN\", \"CONTACT\", \"CIRCLE\", \"OWNER\", \"ADMIN\"]' COMMENT '允许 gets 的角色列表，例如 [\"LOGIN\", \"CONTACT\", \"CIRCLE\", \"OWNER\"]',
  `heads` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '[\"LOGIN\", \"CONTACT\", \"CIRCLE\", \"OWNER\", \"ADMIN\"]' COMMENT '允许 heads 的角色列表，例如 [\"LOGIN\", \"CONTACT\", \"CIRCLE\", \"OWNER\"]',
  `post` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '[\"OWNER\", \"ADMIN\"]' COMMENT '允许 post 的角色列表，例如 [\"LOGIN\", \"CONTACT\", \"CIRCLE\", \"OWNER\"]',
  `put` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '[\"OWNER\", \"ADMIN\"]' COMMENT '允许 put 的角色列表，例如 [\"LOGIN\", \"CONTACT\", \"CIRCLE\", \"OWNER\"]',
  `delete` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '[\"OWNER\", \"ADMIN\"]' COMMENT '允许 delete 的角色列表，例如 [\"LOGIN\", \"CONTACT\", \"CIRCLE\", \"OWNER\"]',
  `date` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `detail` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `alias_UNIQUE`(`alias`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 44 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限配置(必须)' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of Access
-- ----------------------------

-- ----------------------------
-- Table structure for Category
-- ----------------------------
DROP TABLE IF EXISTS `Category`;
CREATE TABLE `Category`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '目录名称',
  `table_id` bigint(20) NOT NULL COMMENT '主键id',
  `generateTime` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of Category
-- ----------------------------
INSERT INTO `Category` VALUES (1, '测试11', 18, '2022-12-08 15:13:23');
INSERT INTO `Category` VALUES (4, '发射点犯得上', 19, '2022-12-09 18:04:12');

-- ----------------------------
-- Table structure for Function
-- ----------------------------
DROP TABLE IF EXISTS `Function`;
CREATE TABLE `Function`  (
  `id` bigint(15) NOT NULL AUTO_INCREMENT,
  `debug` tinyint(2) NOT NULL DEFAULT 0 COMMENT '是否为 DEBUG 调试数据，只允许在开发环境使用，测试和线上环境禁用：0-否，1-是。',
  `userId` bigint(15) NOT NULL COMMENT '管理员用户Id',
  `type` tinyint(2) NOT NULL DEFAULT 0 COMMENT '类型：0-远程函数；1- SQL 函数',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '方法名',
  `returntype` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'Object' COMMENT '返回值类型。TODO RemoteFunction 校验 type 和 back',
  `arguments` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数列表，每个参数的类型都是 String。\n用 , 分割的字符串 比 [JSONArray] 更好，例如 array,item ，更直观，还方便拼接函数。',
  `demo` json NOT NULL COMMENT '可用的示例。\nTODO 改成 call，和返回值示例 back 对应。',
  `detail` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '详细描述',
  `version` tinyint(4) NOT NULL DEFAULT 0 COMMENT '允许的最低版本号，只限于GET,HEAD外的操作方法。\nTODO 使用 requestIdList 替代 version,tag,methods',
  `tag` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '允许的标签.\nnull - 允许全部\nTODO 使用 requestIdList 替代 version,tag,methods',
  `methods` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '允许的操作方法。\nnull - 允许全部\nTODO 使用 requestIdList 替代 version,tag,methods',
  `date` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `return` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '返回值示例',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '远程函数。强制在启动时校验所有demo是否能正常运行通过' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of Function
-- ----------------------------
INSERT INTO `Function` VALUES (3, 0, 0, 0, 'countArray', 'Object', 'array', '{\"array\": [1, 2, 3]}', '获取数组长度。没写调用键值对，会自动补全 \"result()\": \"countArray(array)\"', 0, NULL, NULL, '2018-10-13 16:23:23', NULL);
INSERT INTO `Function` VALUES (4, 0, 0, 0, 'countObject', 'Object', 'object', '{\"object\": {\"key0\": 1, \"key1\": 2}}', '获取对象长度。', 0, NULL, NULL, '2018-10-13 16:23:23', NULL);
INSERT INTO `Function` VALUES (5, 0, 0, 0, 'isContain', 'Object', 'array,value', '{\"array\": [1, 2, 3], \"value\": 2}', '判断是否数组包含值。', 0, NULL, NULL, '2018-10-13 16:23:23', NULL);
INSERT INTO `Function` VALUES (6, 0, 0, 0, 'isContainKey', 'Object', 'object,key', '{\"key\": \"id\", \"object\": {\"id\": 1}}', '判断是否对象包含键。', 0, NULL, NULL, '2018-10-13 16:30:31', NULL);
INSERT INTO `Function` VALUES (7, 0, 0, 0, 'isContainValue', 'Object', 'object,value', '{\"value\": 1, \"object\": {\"id\": 1}}', '判断是否对象包含值。', 0, NULL, NULL, '2018-10-13 16:30:31', NULL);
INSERT INTO `Function` VALUES (8, 0, 0, 0, 'getFromArray', 'Object', 'array,position', '{\"array\": [1, 2, 3], \"result()\": \"getFromArray(array,1)\"}', '根据下标获取数组里的值。position 传数字时直接作为值，而不是从所在对象 request 中取值', 0, NULL, NULL, '2018-10-13 16:30:31', NULL);
INSERT INTO `Function` VALUES (9, 0, 0, 0, 'getFromObject', 'Object', 'object,key', '{\"key\": \"id\", \"object\": {\"id\": 1}}', '根据键获取对象里的值。', 0, NULL, NULL, '2018-10-13 16:30:31', NULL);

-- ----------------------------
-- Table structure for GenBaseClass
-- ----------------------------
DROP TABLE IF EXISTS `GenBaseClass`;
CREATE TABLE `GenBaseClass`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `package_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '基类包名',
  `code` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '基类编码',
  `fields` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '基类字段，多个用英文逗号分隔',
  `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '基类管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of GenBaseClass
-- ----------------------------
INSERT INTO `GenBaseClass` VALUES (3, 'net.maku.framework.common.entity', 'BaseEntity', 'id,creator,create_time,updater,update_time,version,deleted', '使用该基类，则需要表里有这些字段', '2022-11-23 02:38:51');

-- ----------------------------
-- Table structure for GenDatasource
-- ----------------------------
DROP TABLE IF EXISTS `GenDatasource`;
CREATE TABLE `GenDatasource`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `db_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '数据库类型',
  `conn_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '连接名',
  `conn_url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'URL',
  `username` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '数据源管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of GenDatasource
-- ----------------------------
INSERT INTO `GenDatasource` VALUES (1, 'MySQL', 'apijson', 'jdbc:mysql://121.37.109.174:3306/apijson2', 'root', '123456321321', '2022-11-23 02:42:05');
INSERT INTO `GenDatasource` VALUES (3, 'MySQL', '测试', 'http', 'root3123123', 'etete萨达v', '2022-12-03 21:58:32');
INSERT INTO `GenDatasource` VALUES (4, 'MySQL', '测试连接', 'jdbc:mysql://gz-cynosdbmysql-grp-q0u8k5ub.sql.tencentcdb.com:20559/maku_generator?serverTimezone=GMT%2B8', 'root', 'zhf5201314..', '2022-12-04 00:28:13');

-- ----------------------------
-- Table structure for GenFieldType
-- ----------------------------
DROP TABLE IF EXISTS `GenFieldType`;
CREATE TABLE `GenFieldType`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `column_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字段类型',
  `attr_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '属性类型',
  `package_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '属性包名',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `column_type`(`column_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 32 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字段类型管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of GenFieldType
-- ----------------------------
INSERT INTO `GenFieldType` VALUES (1, 'datetime', 'Date', 'java.util.Date', '2022-11-23 02:38:51');
INSERT INTO `GenFieldType` VALUES (2, 'date', 'Date', 'java.util.Date', '2022-11-23 02:38:51');
INSERT INTO `GenFieldType` VALUES (3, 'tinyint', 'Integer', NULL, '2022-11-23 02:38:51');
INSERT INTO `GenFieldType` VALUES (4, 'smallint', 'Integer', NULL, '2022-11-23 02:38:51');
INSERT INTO `GenFieldType` VALUES (5, 'mediumint', 'Integer', NULL, '2022-11-23 02:38:51');
INSERT INTO `GenFieldType` VALUES (6, 'int', 'Integer', NULL, '2022-11-23 02:38:51');
INSERT INTO `GenFieldType` VALUES (7, 'integer', 'Integer', NULL, '2022-11-23 02:38:51');
INSERT INTO `GenFieldType` VALUES (8, 'bigint', 'Long', NULL, '2022-11-23 02:38:51');
INSERT INTO `GenFieldType` VALUES (9, 'float', 'Float', NULL, '2022-11-23 02:38:51');
INSERT INTO `GenFieldType` VALUES (10, 'double', 'Double', NULL, '2022-11-23 02:38:51');
INSERT INTO `GenFieldType` VALUES (11, 'decimal', 'BigDecimal', 'java.math.BigDecimal', '2022-11-23 02:38:51');
INSERT INTO `GenFieldType` VALUES (12, 'bit', 'Boolean', NULL, '2022-11-23 02:38:51');
INSERT INTO `GenFieldType` VALUES (13, 'char', 'String', NULL, '2022-11-23 02:38:51');
INSERT INTO `GenFieldType` VALUES (14, 'varchar', 'String', NULL, '2022-11-23 02:38:51');
INSERT INTO `GenFieldType` VALUES (15, 'tinytext', 'String', NULL, '2022-11-23 02:38:51');
INSERT INTO `GenFieldType` VALUES (16, 'text', 'String', NULL, '2022-11-23 02:38:51');
INSERT INTO `GenFieldType` VALUES (17, 'mediumtext', 'String', NULL, '2022-11-23 02:38:51');
INSERT INTO `GenFieldType` VALUES (18, 'longtext', 'String', NULL, '2022-11-23 02:38:51');
INSERT INTO `GenFieldType` VALUES (19, 'timestamp', 'Date', 'java.util.Date', '2022-11-23 02:38:51');
INSERT INTO `GenFieldType` VALUES (20, 'NUMBER', 'Integer', NULL, '2022-11-23 02:38:51');
INSERT INTO `GenFieldType` VALUES (21, 'BINARY_INTEGER', 'Integer', NULL, '2022-11-23 02:38:51');
INSERT INTO `GenFieldType` VALUES (22, 'BINARY_FLOAT', 'Float', NULL, '2022-11-23 02:38:51');
INSERT INTO `GenFieldType` VALUES (23, 'BINARY_DOUBLE', 'Double', NULL, '2022-11-23 02:38:51');
INSERT INTO `GenFieldType` VALUES (24, 'VARCHAR2', 'String', NULL, '2022-11-23 02:38:51');
INSERT INTO `GenFieldType` VALUES (25, 'NVARCHAR', 'String', NULL, '2022-11-23 02:38:51');
INSERT INTO `GenFieldType` VALUES (26, 'NVARCHAR2', 'String', NULL, '2022-11-23 02:38:51');
INSERT INTO `GenFieldType` VALUES (27, 'CLOB', 'String', NULL, '2022-11-23 02:38:51');
INSERT INTO `GenFieldType` VALUES (28, 'int8', 'Long', NULL, '2022-11-23 02:38:51');
INSERT INTO `GenFieldType` VALUES (29, 'int4', 'Integer', NULL, '2022-11-23 02:38:51');
INSERT INTO `GenFieldType` VALUES (30, 'int2', 'Integer', NULL, '2022-11-23 02:38:51');
INSERT INTO `GenFieldType` VALUES (31, 'numeric', 'BigDecimal', 'java.math.BigDecimal', '2022-11-23 02:38:51');

-- ----------------------------
-- Table structure for GenProjectModify
-- ----------------------------
DROP TABLE IF EXISTS `GenProjectModify`;
CREATE TABLE `GenProjectModify`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `project_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '项目名',
  `project_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '项目标识',
  `project_package` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '项目包名',
  `project_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '项目路径',
  `modify_project_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '变更项目名',
  `modify_project_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '变更标识',
  `modify_project_package` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '变更包名',
  `exclusions` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '排除文件',
  `modify_suffix` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '变更文件',
  `modify_tmp_path` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '变更临时路径',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '项目名变更' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of GenProjectModify
-- ----------------------------
INSERT INTO `GenProjectModify` VALUES (1, 'maku-boot', 'maku', 'net.maku', 'D:/makunet/maku-boot', 'baba-boot', 'baba', 'com.baba', '.git,.idea,target,logs', 'java,xml,yml,txt', NULL, '2022-11-23 02:38:51');
INSERT INTO `GenProjectModify` VALUES (2, 'maku-cloud', 'maku', 'net.maku', 'D:/makunet/maku-cloud', 'baba-cloud', 'baba', 'com.baba', '.git,.idea,target,logs', 'java,xml,yml,txt', NULL, '2022-11-23 02:38:51');

-- ----------------------------
-- Table structure for GenTable
-- ----------------------------
DROP TABLE IF EXISTS `GenTable`;
CREATE TABLE `GenTable`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `table_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表名',
  `class_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类名',
  `table_comment` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '说明',
  `author` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '作者',
  `email` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `package_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '项目包名',
  `version` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '项目版本号',
  `generator_type` tinyint(4) NULL DEFAULT NULL COMMENT '生成方式  0：zip压缩包   1：自定义目录',
  `backend_path` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '后端生成路径',
  `frontend_path` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '前端生成路径',
  `module_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '模块名',
  `function_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '功能名',
  `form_layout` tinyint(4) NULL DEFAULT NULL COMMENT '表单布局  1：一列   2：两列',
  `datasource_id` bigint(20) NULL DEFAULT NULL COMMENT '数据源ID',
  `baseclass_id` bigint(20) NULL DEFAULT NULL COMMENT '基类ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `table_name`(`table_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of GenTable
-- ----------------------------
INSERT INTO `GenTable` VALUES (2, 'apijson_privacy', 'ApijsonPrivacy', '用户隐私信息表。\r\n对安全要求高，不想泄漏真实名称。对外名称为 Privacy', '阿沐', 'babamu@126.com', 'net.maku', '1.0.0', 0, 'D:\\generator\\maku-boot\\maku-server', 'D:\\generator\\maku-admin', 'maku', 'privacy', 1, 1, NULL, '2022-11-23 02:42:29');
INSERT INTO `GenTable` VALUES (3, 'apijson_user', 'ApijsonUser', '用户公开信息表。\r\n对安全要求高，不想泄漏真实名称。对外名称为 User', '阿沐', 'babamu@126.com', 'net.maku', '1.0.0', 0, 'D:\\generator\\maku-boot\\maku-server', 'D:\\generator\\maku-admin', 'maku', 'user', 1, 1, NULL, '2022-11-23 02:42:29');
INSERT INTO `GenTable` VALUES (4, 'BalanceTransfers', 'BalanceTransfers', '', '阿沐', 'babamu@126.com', 'net.maku', '1.0.0', 0, 'D:\\generator\\maku-boot\\maku-server', 'D:\\generator\\maku-admin', 'maku', 'BalanceTransfers', 1, 1, NULL, '2022-11-23 02:42:30');
INSERT INTO `GenTable` VALUES (5, 'ChangeMoney', 'ChangeMoney', '', '阿沐', 'babamu@126.com', 'net.maku', '1.0.0', 0, 'D:\\generator\\maku-boot\\maku-server', 'D:\\generator\\maku-admin', 'maku', 'ChangeMoney', 1, 1, NULL, '2022-11-23 02:42:31');
INSERT INTO `GenTable` VALUES (6, 'ClubProfitRecord', 'ClubProfitRecord', 'aaa', '阿沐', 'babamu@126.com', 'net.maku', '1.0.0', 0, 'D:\\generator\\maku-boot\\maku-server', 'D:\\generator\\maku-admin', 'maku', 'ClubProfitRecord', 1, 1, NULL, '2022-11-23 02:42:32');
INSERT INTO `GenTable` VALUES (7, 'Comment', 'Comment', '评论', '阿沐', 'babamu@126.com', 'net.maku', '1.0.0', 0, 'D:\\generator\\maku-boot\\maku-server', 'D:\\generator\\maku-admin', 'maku', 'Comment', 1, 1, NULL, '2022-11-23 02:42:33');
INSERT INTO `GenTable` VALUES (8, 'Document', 'Document', '测试用例文档\n后端开发者在测试好后，把选好的测试用例上传，这样就能共享给前端/客户端开发者', '阿沐', 'babamu@126.com', 'net.maku', '1.0.0', 0, 'D:\\generator\\maku-boot\\maku-server', 'D:\\generator\\maku-admin', 'maku', 'Document', 1, 1, NULL, '2022-11-23 02:42:33');
INSERT INTO `GenTable` VALUES (9, 'ExchangeRate', 'ExchangeRate', '', '阿沐', 'babamu@126.com', 'net.maku', '1.0.0', 0, 'D:\\generator\\maku-boot\\maku-server', 'D:\\generator\\maku-admin', 'maku', 'ExchangeRate', 1, 1, NULL, '2022-11-23 02:42:34');
INSERT INTO `GenTable` VALUES (10, 'Function', 'Function', '远程函数。强制在启动时校验所有demo是否能正常运行通过', '阿沐', 'babamu@126.com', 'net.maku', '1.0.0', 0, 'D:\\generator\\maku-boot\\maku-server', 'D:\\generator\\maku-admin', 'maku', 'Function', 1, 1, NULL, '2022-11-23 02:42:35');
INSERT INTO `GenTable` VALUES (13, 'TestRecord', 'TestRecord', '测试记录(必须)\n主要用于保存自动化接口回归测试。5.0.0 可能改为 Test(先废弃 Test 表) ', '阿沐', 'babamu@126.com', 'net.maku', '1.0.0', 0, 'D:\\generator\\maku-boot\\maku-server', 'D:\\generator\\maku-admin', 'maku', 'TestRecord', 1, 1, NULL, '2022-11-23 02:42:54');
INSERT INTO `GenTable` VALUES (14, 'ShareProfitRecord', 'ShareProfitRecord', 'w', '阿沐', 'babamu@126.com', 'net.maku', '1.0.0', 1, 'D:\\generator\\maku-boot\\maku-server', 'D:\\generator\\maku-admin', 'maku', 'ShareProfitRecord', 1, 1, NULL, '2022-11-23 02:42:55');
INSERT INTO `GenTable` VALUES (15, 'gen_test_member', 'GenTestMember', '测试1', '阿沐', 'babamu@126.com', 'net.maku', '1.0.0', 0, 'D:\\generator\\maku-boot\\maku-server', 'D:\\generator\\maku-admin', 'maku', 'member', 1, 0, NULL, '2022-11-23 11:14:46');
INSERT INTO `GenTable` VALUES (16, 'gen_test_student', 'GenTestStudent', '测试2', '阿沐', 'babamu@126.com', 'net.maku', '1.0.0', 0, 'D:\\generator\\maku-boot\\maku-server', 'D:\\generator\\maku-admin', 'maku', 'student', 1, 0, NULL, '2022-11-23 11:16:31');
INSERT INTO `GenTable` VALUES (17, 'gen_base_class', 'GenBaseClass', '基类管理', '阿沐', 'babamu@126.com', 'net.maku', '1.0.0', 0, 'D:\\generator\\maku-boot\\maku-server', 'D:\\generator\\maku-admin', 'maku', 'class', 1, 4, NULL, '2022-12-05 21:18:27');
INSERT INTO `GenTable` VALUES (18, 'MyTest', 'MyTest', '', '阿沐', 'babamu@126.com', 'net.maku', '1.0.0', 0, 'D:\\generator\\maku-boot\\maku-server', 'D:\\generator\\maku-admin', 'maku', 'MyTest', 1, 4, NULL, '2022-12-08 00:56:05');
INSERT INTO `GenTable` VALUES (19, 'Test111', 'Test111', '', '阿沐', 'babamu@126.com', 'net.maku', '1.0.0', 0, 'D:\\generator\\maku-boot\\maku-server', 'D:\\generator\\maku-admin', 'maku', 'Test111', 1, 4, NULL, '2022-12-09 15:33:22');

-- ----------------------------
-- Table structure for GenTableField
-- ----------------------------
DROP TABLE IF EXISTS `GenTableField`;
CREATE TABLE `GenTableField`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `table_id` bigint(20) NULL DEFAULT NULL COMMENT '表ID',
  `field_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字段名称',
  `field_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字段类型',
  `field_comment` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字段说明',
  `attr_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '属性名',
  `attr_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '属性类型',
  `package_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '属性包名',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `auto_fill` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '自动填充  DEFAULT、INSERT、UPDATE、INSERT_UPDATE',
  `primary_pk` tinyint(4) NULL DEFAULT NULL COMMENT '主键 0：否  1：是',
  `base_field` tinyint(4) NULL DEFAULT NULL COMMENT '基类字段 0：否  1：是',
  `form_item` tinyint(4) NULL DEFAULT NULL COMMENT '表单项 0：否  1：是',
  `form_required` tinyint(4) NULL DEFAULT NULL COMMENT '表单必填 0：否  1：是',
  `form_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表单类型',
  `form_dict` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表单字典类型',
  `form_validator` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表单效验',
  `grid_item` tinyint(4) NULL DEFAULT NULL COMMENT '列表项 0：否  1：是',
  `grid_sort` tinyint(4) NULL DEFAULT NULL COMMENT '列表排序 0：否  1：是',
  `query_item` tinyint(4) NULL DEFAULT NULL COMMENT '查询项 0：否  1：是',
  `query_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '查询方式',
  `query_form_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '查询表单类型',
  `query_dict` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '查询字典类型',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 151 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成表字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of GenTableField
-- ----------------------------
INSERT INTO `GenTableField` VALUES (14, 2, 'id', 'bigint', '唯一标识', 'id', 'Long', NULL, 0, 'DEFAULT', 1, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (15, 2, 'certified', 'tinyint', '已认证', 'certified', 'Integer', NULL, 1, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (16, 2, 'phone', 'bigint', '手机号，仅支持 11 位数的。不支持 +86 这种国家地区开头的。如果要支持就改为 VARCHAR(14)', 'phone', 'Long', NULL, 2, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (17, 2, 'balance', 'decimal', '余额', 'balance', 'BigDecimal', 'java.math.BigDecimal', 3, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (18, 2, '_password', 'varchar', '登录密码', 'Password', 'String', NULL, 4, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (19, 2, '_payPassword', 'int', '支付密码', 'Paypassword', 'Integer', NULL, 5, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (20, 2, 'email', 'varchar', '邮箱', 'email', 'String', NULL, 6, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (21, 2, 'name', 'varchar', '用户名', 'name', 'String', NULL, 7, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (22, 2, 'show_id', 'varchar', '自定义id', 'showId', 'String', NULL, 8, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (23, 2, 'introduce_code', 'varchar', '推荐码', 'introduceCode', 'String', NULL, 9, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (24, 3, 'id', 'bigint', '唯一标识', 'id', 'Long', NULL, 0, 'DEFAULT', 1, 0, 1, 0, 'text', NULL, NULL, 1, 1, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (25, 3, 'sex', 'tinyint', '性别：\n0-男\n1-女', 'sex', 'Integer', NULL, 1, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 1, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (26, 3, 'name', 'varchar', '名称', 'name', 'String', NULL, 2, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (27, 3, 'tag', 'varchar', '标签', 'tag', 'String', NULL, 3, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (28, 3, 'head', 'varchar', '头像url', 'head', 'String', NULL, 4, 'DEFAULT', 0, 0, 1, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (29, 3, 'contactIdList', 'json', '联系人id列表', 'contactIdList', 'Object', NULL, 5, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (30, 3, 'pictureList', 'json', '照片列表', 'pictureList', 'Object', NULL, 6, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (31, 3, 'date', 'timestamp', '创建日期', 'date', 'Date', 'java.util.Date', 7, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (32, 3, 'enable', 'varchar', '是否可以', 'enable', 'String', NULL, 8, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (33, 4, 'id', 'int', '', 'id', 'Integer', NULL, 0, 'DEFAULT', 1, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (34, 4, 'from', 'varchar', '', 'from', 'String', NULL, 1, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (35, 4, 'to', 'varchar', '', 'to', 'String', NULL, 2, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (36, 4, 'money', 'varchar', '', 'money', 'String', NULL, 3, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (37, 5, 'id', 'bigint', '', 'id', 'Long', NULL, 0, 'DEFAULT', 1, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (38, 5, 'userId', 'bigint', '', 'userId', 'Long', NULL, 1, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (39, 5, 'money', 'double', '金额', 'money', 'Double', NULL, 2, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (40, 5, 'from_type', 'varchar', '原来货币', 'fromType', 'String', NULL, 3, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (41, 5, 'to_type', 'varchar', '目标货币', 'toType', 'String', NULL, 4, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (42, 5, 'status', 'varchar', '状态，是否处理', 'status', 'String', NULL, 5, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (43, 5, 'generate_time', 'datetime', '', 'generateTime', 'Date', 'java.util.Date', 6, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (44, 6, 'id', 'int', '', 'id', 'Integer', NULL, 0, 'DEFAULT', 1, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (45, 6, 'userId', 'bigint', '', 'userId', 'Long', NULL, 1, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (46, 6, 'money', 'double', '分红', 'money', 'Double', NULL, 2, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (47, 6, 'created_at', 'timestamp', '分红发放时间', 'createdAt', 'Date', 'java.util.Date', 3, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (48, 7, 'id', 'bigint', '唯一标识', 'id', 'Long', NULL, 0, 'DEFAULT', 1, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (49, 7, 'toId', 'bigint', '被回复的id', 'toId', 'Long', NULL, 1, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (50, 7, 'userId', 'bigint', '评论人 User 的 id', 'userId', 'Long', NULL, 2, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (51, 7, 'momentId', 'bigint', '动态id', 'momentId', 'Long', NULL, 3, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (52, 7, 'date', 'timestamp', '创建日期', 'date', 'Date', 'java.util.Date', 4, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (53, 7, 'content', 'varchar', '内容', 'content', 'String', NULL, 5, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (54, 8, 'id', 'bigint', '唯一标识', 'id', 'Long', NULL, 0, 'DEFAULT', 1, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (55, 8, 'debug', 'tinyint', '是否为 DEBUG 调试数据，只允许在开发环境使用，测试和线上环境禁用：0-否，1-是。', 'debug', 'Integer', NULL, 1, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (56, 8, 'userId', 'bigint', '管理员用户id。\n需要先建Admin表，新增登录等相关接口。', 'userId', 'Long', NULL, 2, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (57, 8, 'testAccountId', 'bigint', '测试账号id。0-不限', 'testAccountId', 'Long', NULL, 3, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (58, 8, 'version', 'tinyint', '接口版本号\n<=0 - 不限制版本，任意版本都可用这个接口；\n>0 - 在这个版本添加的接口。\n\n可在给新版文档前调高默认值，新增的测试用例就不用手动设置版本号了。', 'version', 'Integer', NULL, 4, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (59, 8, 'name', 'varchar', '接口名称', 'name', 'String', NULL, 5, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (60, 8, 'type', 'varchar', 'PARAM - GET  url parameters,\nFORM - POST  application/www-x-form-url-encoded,\nJSON - POST  application/json', 'type', 'String', NULL, 6, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (61, 8, 'url', 'varchar', '请求地址', 'url', 'String', NULL, 7, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (62, 8, 'request', 'text', '请求\n用json格式会导致强制排序，而请求中引用赋值只能引用上面的字段，必须有序。', 'request', 'String', NULL, 8, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (63, 8, 'apijson', 'text', '从 request 映射为实际的 APIJSON 请求 JSON', 'apijson', 'String', NULL, 9, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (64, 8, 'sqlauto', 'text', '为 SQLAuto 留的字段，格式为\\\\\\\\n{“sql”:”SELECT * FROM `sys`.`Comment` LIMIT ${limit}”,”arg”:”limit: 3”}', 'sqlauto', 'String', NULL, 10, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (65, 8, 'standard', 'text', '', 'standard', 'String', NULL, 11, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (66, 8, 'header', 'text', '请求头 Request Header：\nkey: value  //注释', 'header', 'String', NULL, 12, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (67, 8, 'date', 'timestamp', '创建日期', 'date', 'Date', 'java.util.Date', 13, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (68, 8, 'detail', 'text', '详细的说明，可以是普通文本或 Markdown 格式文本', 'detail', 'String', NULL, 14, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (69, 9, 'id', 'int', '', 'id', 'Integer', NULL, 0, 'DEFAULT', 1, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (70, 9, 'from', 'varchar', '币1', 'from', 'String', NULL, 1, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (71, 9, 'to', 'varchar', '币2', 'to', 'String', NULL, 2, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (72, 9, 'rate', 'double', '', 'rate', 'Double', NULL, 3, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (73, 10, 'id', 'bigint', '', 'id', 'Long', NULL, 0, 'DEFAULT', 1, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (74, 10, 'debug', 'tinyint', '是否为 DEBUG 调试数据，只允许在开发环境使用，测试和线上环境禁用：0-否，1-是。', 'debug', 'Integer', NULL, 1, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (75, 10, 'userId', 'bigint', '管理员用户Id', 'userId', 'Long', NULL, 2, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (76, 10, 'type', 'tinyint', '类型：0-远程函数；1- SQL 函数', 'type', 'Integer', NULL, 3, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (77, 10, 'name', 'varchar', '方法名', 'name', 'String', NULL, 4, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (78, 10, 'returntype', 'varchar', '返回值类型。TODO RemoteFunction 校验 type 和 back', 'returntype', 'String', NULL, 5, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (79, 10, 'arguments', 'varchar', '参数列表，每个参数的类型都是 String。\n用 , 分割的字符串 比 [JSONArray] 更好，例如 array,item ，更直观，还方便拼接函数。', 'arguments', 'String', NULL, 6, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (80, 10, 'demo', 'json', '可用的示例。\nTODO 改成 call，和返回值示例 back 对应。', 'demo', 'Object', NULL, 7, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (81, 10, 'detail', 'varchar', '详细描述', 'detail', 'String', NULL, 8, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (82, 10, 'version', 'tinyint', '允许的最低版本号，只限于GET,HEAD外的操作方法。\nTODO 使用 requestIdList 替代 version,tag,methods', 'version', 'Integer', NULL, 9, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (83, 10, 'tag', 'varchar', '允许的标签.\nnull - 允许全部\nTODO 使用 requestIdList 替代 version,tag,methods', 'tag', 'String', NULL, 10, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (84, 10, 'methods', 'varchar', '允许的操作方法。\nnull - 允许全部\nTODO 使用 requestIdList 替代 version,tag,methods', 'methods', 'String', NULL, 11, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (85, 10, 'date', 'timestamp', '创建时间', 'date', 'Date', 'java.util.Date', 12, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (86, 10, 'return', 'varchar', '返回值示例', 'return', 'String', NULL, 13, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (106, 13, 'id', 'bigint', '唯一标识', 'id', 'Long', NULL, 0, 'DEFAULT', 1, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (107, 13, 'headless', 'tinyint', '是否是用于 CI/CD 的无头执行结果，而不是通过 APIAuto 等前端网页 UI 来执行', 'headless', 'Integer', NULL, 1, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (108, 13, 'userId', 'bigint', '用户id', 'userId', 'Long', NULL, 2, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (109, 13, 'testAccountId', 'bigint', '', 'testAccountId', 'Long', NULL, 3, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (110, 13, 'documentId', 'bigint', '测试用例文档id', 'documentId', 'Long', NULL, 4, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (111, 13, 'randomId', 'bigint', '随机配置 id', 'randomId', 'Long', NULL, 5, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (112, 13, 'host', 'varchar', '', 'host', 'String', NULL, 6, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (113, 13, 'date', 'timestamp', '创建日期', 'date', 'Date', 'java.util.Date', 7, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (114, 13, 'duration', 'bigint', '服务处理耗时，单位为毫秒(ms)', 'duration', 'Long', NULL, 8, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (115, 13, 'minDuration', 'bigint', '统计过的最短服务处理耗时，单位为毫秒(ms)', 'minDuration', 'Long', NULL, 9, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (116, 13, 'maxDuration', 'bigint', '统计过的最长服务处理耗时，单位为毫秒(ms)', 'maxDuration', 'Long', NULL, 10, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (117, 13, 'response', 'text', '接口返回结果JSON', 'response', 'String', NULL, 11, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (118, 13, 'compare', 'text', '对比结果', 'compare', 'String', NULL, 12, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (119, 13, 'standard', 'text', 'response 的校验标准，是一个 JSON 格式的 AST ，描述了正确 Response 的结构、里面的字段名称、类型、长度、取值范围 等属性。', 'standard', 'String', NULL, 13, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (120, 14, 'id', 'int', '', 'id', 'Integer', NULL, 0, 'DEFAULT', 1, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (121, 14, 'userId', 'bigint', '', 'userId', 'Long', NULL, 1, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (122, 14, 'money', 'double', '', 'money', 'Double', NULL, 2, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (123, 14, 'generate_time', 'datetime', '', 'generateTime', 'Date', 'java.util.Date', 3, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (124, 15, 'member_id', 'bigint', '会员ID', 'memberId', 'Long', NULL, 0, 'DEFAULT', 1, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (125, 15, 'name', 'varchar', '姓名', 'name', 'String', NULL, 1, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (126, 15, 'gender', 'tinyint', '性别', 'gender', 'Integer', NULL, 2, 'DEFAULT', 0, 0, 0, 0, 'number', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (127, 15, 'age', 'int', '年龄', 'age', 'Integer', NULL, 3, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (128, 15, 'occupation', 'varchar', '职业', 'occupation', 'String', NULL, 4, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (129, 15, 'create_time', 'datetime', '创建时间', 'createTime', 'Date', 'java.util.Date', 5, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (130, 16, 'id', 'bigint', '学生ID', 'id', 'Long', NULL, 0, 'DEFAULT', 1, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (131, 16, 'name', 'varchar', '姓名', 'name', 'String', NULL, 1, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (132, 16, 'gender', 'tinyint', '性别', 'gender', 'Integer', NULL, 2, 'DEFAULT', 0, 0, 0, 0, 'number', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (133, 16, 'age', 'int', '年龄', 'age', 'Integer', NULL, 3, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (134, 16, 'class_name', 'varchar', '班级', 'className', 'String', NULL, 4, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (135, 16, 'version', 'int', '版本号', 'version', 'Integer', NULL, 5, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (136, 16, 'deleted', 'tinyint', '删除标识', 'deleted', 'Integer', NULL, 6, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (137, 16, 'creator', 'bigint', '创建者', 'creator', 'Long', NULL, 7, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (138, 16, 'create_time', 'datetime', '创建时间', 'createTime', 'Date', 'java.util.Date', 8, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (139, 16, 'updater', 'bigint', '更新者', 'updater', 'Long', NULL, 9, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (140, 16, 'update_time', 'datetime', '更新时间', 'updateTime', 'Date', 'java.util.Date', 10, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (141, 17, 'id', 'bigint', 'id', 'id', 'Long', NULL, 0, 'DEFAULT', 1, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (142, 17, 'package_name', 'varchar', '基类包名', 'packageName', 'String', NULL, 1, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (143, 17, 'code', 'varchar', '基类编码', 'code', 'String', NULL, 2, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (144, 17, 'fields', 'varchar', '基类字段，多个用英文逗号分隔', 'fields', 'String', NULL, 3, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (145, 17, 'remark', 'varchar', '备注', 'remark', 'String', NULL, 4, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (146, 17, 'create_time', 'datetime', '创建时间', 'createTime', 'Date', 'java.util.Date', 5, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (147, 18, 'id', 'int', 'id', 'id', 'Integer', NULL, 0, 'DEFAULT', 1, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (148, 18, 'test', 'varchar', '测试222', 'test', 'String', NULL, 1, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (149, 19, 'id', 'int', '321321', 'id', 'Integer', NULL, 0, 'DEFAULT', 1, 0, 0, 0, 'text', NULL, NULL, 1, 0, 1, '=', 'text', NULL);
INSERT INTO `GenTableField` VALUES (150, 19, 'fsfsd', 'varchar', '312312', 'fsfsd', 'String', NULL, 1, 'DEFAULT', 0, 0, 0, 0, 'text', NULL, NULL, 1, 0, 0, '=', 'text', NULL);

-- ----------------------------
-- Table structure for GenTestMember
-- ----------------------------
DROP TABLE IF EXISTS `GenTestMember`;
CREATE TABLE `GenTestMember`  (
  `member_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '会员ID',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `gender` tinyint(4) NULL DEFAULT NULL COMMENT '性别',
  `age` int(11) NULL DEFAULT NULL COMMENT '年龄',
  `occupation` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '职业',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`member_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '测试1' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of GenTestMember
-- ----------------------------

-- ----------------------------
-- Table structure for GenTestStudent
-- ----------------------------
DROP TABLE IF EXISTS `GenTestStudent`;
CREATE TABLE `GenTestStudent`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '学生ID',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `gender` tinyint(4) NULL DEFAULT NULL COMMENT '性别',
  `age` int(11) NULL DEFAULT NULL COMMENT '年龄',
  `class_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '班级',
  `version` int(11) NULL DEFAULT NULL COMMENT '版本号',
  `deleted` tinyint(4) NULL DEFAULT NULL COMMENT '删除标识',
  `creator` bigint(20) NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updater` bigint(20) NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '测试2' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of GenTestStudent
-- ----------------------------

-- ----------------------------
-- Table structure for MyTest
-- ----------------------------
DROP TABLE IF EXISTS `MyTest`;
CREATE TABLE `MyTest`  (
  `id` int(11) NOT NULL COMMENT 'id',
  `test` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '测试222',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '测试' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of MyTest
-- ----------------------------
INSERT INTO `MyTest` VALUES (2, '321321');
INSERT INTO `MyTest` VALUES (312, '13213312312312');

-- ----------------------------
-- Table structure for Request
-- ----------------------------
DROP TABLE IF EXISTS `Request`;
CREATE TABLE `Request`  (
  `id` bigint(15) NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `debug` tinyint(2) NOT NULL DEFAULT 0 COMMENT '是否为 DEBUG 调试数据，只允许在开发环境使用，测试和线上环境禁用：0-否，1-是。',
  `version` tinyint(4) NOT NULL DEFAULT 1 COMMENT 'GET,HEAD可用任意结构访问任意开放内容，不需要这个字段。\n其它的操作因为写入了结构和内容，所以都需要，按照不同的version选择对应的structure。\n\n自动化版本管理：\nRequest JSON最外层可以传  “version”:Integer 。\n1.未传或 <= 0，用最新版。 “@order”:”version-“\n2.已传且 > 0，用version以上的可用版本的最低版本。 “@order”:”version+”, “version{}”:”>={version}”',
  `method` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'GETS' COMMENT '只限于GET,HEAD外的操作方法。',
  `tag` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标签',
  `structure` json NOT NULL COMMENT '结构。\nTODO 里面的 PUT 改为 UPDATE，避免和请求 PUT 搞混。',
  `detail` varchar(10000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '详细说明',
  `date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1658229984271 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '请求参数校验配置(必须)。\n最好编辑完后删除主键，这样就是只读状态，不能随意更改。需要更改就重新加上主键。\n\n每次启动服务器时加载整个表到内存。\n这个表不可省略，model内注解的权限只是客户端能用的，其它可以保证即便服务端代码错误时也不会误删数据。' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of Request
-- ----------------------------

-- ----------------------------
-- Table structure for Test
-- ----------------------------
DROP TABLE IF EXISTS `Test`;
CREATE TABLE `Test`  (
  `Test` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of Test
-- ----------------------------
INSERT INTO `Test` VALUES ('332');

-- ----------------------------
-- Table structure for Test111
-- ----------------------------
DROP TABLE IF EXISTS `Test111`;
CREATE TABLE `Test111`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '321321',
  `fsfsd` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '312312',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of Test111
-- ----------------------------
INSERT INTO `Test111` VALUES (1, 'fdsfsd');

SET FOREIGN_KEY_CHECKS = 1;
